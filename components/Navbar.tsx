import Link from 'next/link';
import { BsLinkedin } from 'react-icons/bs';
import ActiveLink from './shared/ActiveLink';

export default function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container-fluid">
        <Link className="nav-link" href="/">
          <img
            srcSet="/WideLogo-nav.png 116w, /WideLogo-nav-2x.png 232w"
            height="60px"
            width="116px"
            alt="EBS Tax Credits Logo"
          />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-lg-2 me-auto text-nowrap">
            <li className="nav-item">
              <ActiveLink href="/" text="Home" />
            </li>
            <li className="nav-item mx-1">
              <ActiveLink href="/about" text="About" />
            </li>
            <li className="nav-item mx-1">
              <ActiveLink href="/nmtc" text="NMTCs" />
            </li>
            <li className="nav-item mx-1">
              <ActiveLink href="/portfolio" text="Portfolio" />
            </li>
            <li className="nav-item mx-1">
              <ActiveLink href="/team" text="Our Team" />
            </li>
            <li className="nav-item mx-1">
              <ActiveLink href="/contact" text="Contact Us" />
            </li>
          </ul>
          <ul className="navbar-nav d-flex ms-lg-2 mb-2">
            <li className="nav-item">
              <a
                className="nav-link fs-3"
                href="https://linkedin.com/company/ebs-tax-credits"
                aria-label="Link to LinkedIn"
              >
                <BsLinkedin />
              </a>
            </li>
            {/* <li className="nav-item">
              <a className="nav-link fs-3" href="https://twitter.com/EBSTaxCredits" aria-label="Link to Twitter">
                <BsTwitter />
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link fs-3" href="https://facebook.com/ebstaxcredits" aria-label="Link to Facebook">
                <BsFacebook />
              </a>
            </li> */}
          </ul>
        </div>
      </div>
    </nav>
  );
}
