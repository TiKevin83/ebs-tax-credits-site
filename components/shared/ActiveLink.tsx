import Link from "next/link";
import { useRouter } from "next/router";

export default function ActiveLink({ href, text }: { href: string, text: string}) {
  const router = useRouter();
  return <Link className={`nav-link fs-3${router.pathname === href ? ' active' : ''}`} href={href}>{text}</Link>
}
