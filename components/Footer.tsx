import { BsTwitter } from 'react-icons/bs';
import { SiGitlab } from 'react-icons/si';
import { FaRegCopyright } from 'react-icons/fa';

export default function Footer() {
  return (
    <footer className="mt-3 text-center fs-6 text-muted">
      <hr />
      <p className="d-flex justify-content-center align-items-center mb-0">
        <FaRegCopyright className="me-1" />
        EBS TAX CREDITS 2025
      </p>
      <p className="d-flex justify-content-center align-items-center mb-0">
        Site design by
        <a className="link-dark mx-1" href="https://twitter.com/tikevin83">
          TiKevin83
        </a>
        <BsTwitter />
      </p>
      <p className="d-flex justify-content-center align-items-center">
        Site source available on
        <a className="link-dark mx-1" href="https://gitlab.com/TiKevin83/ebs-tax-credits-site">
          {' '}
          GitLab
        </a>
        <SiGitlab />
      </p>
    </footer>
  );
}
