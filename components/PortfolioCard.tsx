interface PortfolioCardProps {
  image: {
    source: string,
    alt: string,
  },
  name: string,
  tagLine: string,
  tagLineLink: string,
  closingYear: string,
  location: string,
  netProceeds: string,
  budget: string,
  CDEs: string,
  investor: string,
  features: string,
  impacts?: string,
  results?: string,
  services?: string,
  awards?: string,
}

export const PortfolioCard = (props: PortfolioCardProps) => {
  return <div className="col">
  <div className="card h-100">
    <img src={props.image.source} className="card-img-top" alt={props.image.alt} />
    <div className="card-body">
      <h5 className="card-title">{props.name}</h5>
      <p className="text-muted">
        <a href={props.tagLineLink}>{props.tagLine}</a>
        <br />
        {props.location}
        <br />
        {props.closingYear}
      </p>
      <ul className="card-text">
        <li>
          <strong>Net Proceeds:</strong> {props.netProceeds}
        </li>
        <li>
          <strong>Project Budget:</strong> {props.budget}
        </li>
        <li>
          <strong>CDEs:</strong> {props.CDEs}
        </li>
        <li>
          <strong>Investor:</strong> {props.investor}
        </li>
        {props.impacts ? <li>
          <strong>Impacts:</strong> {props.impacts}
        </li> : null}
        <li>
          <strong>Features:</strong> {props.features}
        </li>
        {props.results ? <li>
          <strong>Results:</strong> {props.results}
        </li> : null}
        {props.services ? <li>
          <strong>Services:</strong> {props.services}
        </li> : null}
        {props.awards ? <li>
          <strong>Awards:</strong> {props.awards}
        </li> : null}
      </ul>
    </div>
  </div>
</div>
}