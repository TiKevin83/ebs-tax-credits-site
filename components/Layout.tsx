import React from 'react';
import Footer from './Footer';
import Navbar from './Navbar';

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Navbar />
      <main>
        <div className="container">
          <picture>
            <source
              srcSet="/Background-640.avif 640w, /Background-1280.avif 1280w, /Background-1920.avif 1920w, /Background-3840.avif 3840w"
              type="image/avif"
            />
            <source
              srcSet="/Background-640.jpg 640w, /Background-1280.jpg 1280w, /Background-1920.jpg 1920w, /Background-3840.jpg 3840w"
              type="image/jpeg"
            />
            <img className="w-100" src="/Background-1920.jpg" alt="Backdrop Memphis Union Mission" />
          </picture>
          {children}
        </div>
      </main>
      <Footer />
    </>
  );
}
