import Head from 'next/head';

export default function About() {
  const title = 'EBS Tax Credits - About NMTCs';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="EBS Tax Credits explains NMTCs" />
      </Head>
      <h1 className="mt-3 text-center fw-bold">NMTCs</h1>
      <br />
      <h2>What are New Markets Tax Credits (NMTCs)?</h2>
      <p>
        The New Markets Tax Credit (NMTCs) is a funding source intended to promote services and jobs in specific
        low-income communities. NMTCs are authorized by Congress and administered by the Treasury Department through the
        CDFI Fund. The credits are awarded to Community Development Entities (CDEs) who split their allocation among
        various nonprofit and for-profit projects. Ultimately the funding is delivered by a bank that purchases the tax
        credits. The benefit to a project can be 15-20% of the project's total funding for a budget of at least $10
        million.
      </p>
      <hr />
      <h2>What kinds of projects qualify?</h2>
      <p>
        Community Development Entities and Tax Credit Investors are interested in a variety of projects.  Qualifying projects:
      </p>
      <ul>
        <li>
          include community facilities, health care, education, community food, workforce development, and businesses creating new jobs among others
        </li>
        <li>
          typically are capital projects that include acquisition, new construction, or major rehabilitation
        </li>
        <li>
          can occasionally be the acquisition of new equipment or funding operating costs related to expansions
        </li>
        <li>
          have development costs of at least $10 million, with the majority of other funding secured
        </li>
        <li>
          be located in low income communities as determined by census tract data
        </li>
      </ul>
    </>
  );
}
