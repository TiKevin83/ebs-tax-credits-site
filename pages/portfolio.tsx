import Head from 'next/head';
import { PortfolioCard } from '../components/PortfolioCard';

export default function Portfolio() {
  const title = 'EBS Tax Credits - Portfolio';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="Success Stories at EBS Tax Credits" />
      </Head>
      <h1 className="mt-3 text-center">
        <span className="fw-bold">Success Stories</span> with EBS Tax Credits
      </h1>
      <p>
        The typical EBS project is the new construction or renovation and expansion of a faith-based community facility
        with housing and services for the homeless.
        We have worked in most of the 50 states, typically in major cities such as Los Angeles, Nashville, Atlanta, Fort Worth, Phoenix, Memphis, Pittsburgh, Indianapolis, and Oklahoma City.
        We have worked on many other types of community facilities such as tiny home communities for the chronically homeless in Austin and San Antonio, a charter school in Fresno, an early childhood learning center and clinic in Fort Wayne, an addiction recovery program in Baltimore, an emergency room diversion program in Merced, a YMCA in Oregon, and a school in Dallas.
        Below is a sampling of success stories with EBS Tax Credits.
      </p>
      <div className="row row-cols-1 row-cols-md-2 g-4">
      <PortfolioCard
          image={{ source: "/mobile-loaves-phase3.jpg", alt: "Overhead View of Mobile Loaves and Fishes Phase 3" }}
          name="Community First! Village Phase 3"
          tagLine="Mobile Loaves & Fishes"
          tagLineLink="https://mlf.org/"
          closingYear="2024"
          location="Austin, TX"
          netProceeds="$2.8 Million"
          budget="$30.6 Million"
          CDEs="PeopleFund and Chase New Markets Corporation"
          investor="Chase Bank"
          impacts="Community Expansion with 606 Homes for Formerly Homeless People"
          features="445 micro homes and townhomes, 161 park model homes, 11,837 SF Neighbor Services building to support residents, 5,839 SF Welcome Center/Assembly Hall, 4,048 SF Market, 18,963 SF Corporate Offices building with offices and meeting space, 13 outdoor kitchens, 13 laundry/bathroom facilities, and gardens"
          services="Expansion of existing services including physical and mental healthcare, community events, counseling, addiction recovery, support groups, education services, entrepreneurial opportunities, hospice care, and public replication seminars"
          />
        <PortfolioCard
          image={{ source: "/womens-live-and-learn-center.jpg", alt: "Entrance of the Women's live and learn center" }}
          name="Women’s Live and Learn Center"
          tagLine="YWCA San Antonio"
          tagLineLink="https://ywcasa.org/"
          closingYear="2024"
          location="San Antonio, TX"
          netProceeds="$2.5 Million"
          budget="$16 Million"
          CDEs="PeopleFund"
          investor="Truist"
          impacts="48-Seat Early Childhood Center & 30 Transitional Housing Units"
          features="Early childhood center with 48 seats for children ages 0-5, offices, communal gathering spaces, and 30 units of transitional housing for women ages 18-25 who are at-risk of homelessness, domestic violence survivors, or aging out of foster care"
          services="Trauma-informed care, case management, mental health services, workforce training and navigation, community health worker training, health screenings, vaccines, senior connection program, immigrant services, childcare, education, financial counseling, and business workshops"
          />
      </div>
      <hr className="border border-4 border-primary rounded-pill" />
      <div className="row row-cols-1 row-cols-md-2 g-4">
        <PortfolioCard
          image={{ source: "/city-center.jpg", alt: "Concept art of City Center in Fresno" }}
          name="City Center Phases 1 and 2"
          tagLine="Fresno Mission"
          tagLineLink="https://www.fresnomission.org/"
          closingYear="2022 and 2023"
          location="Fresno, CA"
          netProceeds="$5.2 Million"
          budget="$40 Million"
          CDEs="Accion Opportunity Fund, New Markets Community Capital (TELACU), and PNC Community Partners"
          investor="PNC Bank"
          impacts="Community Hub Serving 1,000+ Youth and Families Annually"
          features="Charter school for at-risk youth, crisis housing, health center, social service offices, café, youth center, outdoor recreation facilities, and community playground"
          services="Free choice food market, salon and barber shop, counseling, addiction recovery, job training, financial literacy, bilingual advocacy services, domestic abuse services, and housing advocacy"
          />
        <PortfolioCard
          image={{ source: "/withnell-family-ymca.jpg", alt: "Concept Art of the Salem Family YMCA Project" }}
          name="Withnell Family YMCA"
          tagLine="The Family YMCA of Marion & Polk Counties"
          tagLineLink="https://theyonline.org/"
          closingYear="2021"
          location="Salem, OR"
          netProceeds="$4.3 Million"
          budget="$29.8 Million"
          CDEs="National Community Fund (United Fund Advisors)"
          investor="US Bank"
          impacts="Community Center Serving 2,400 Individuals Annually"
          features="Accessible warm water pool, saunas, gym, multipurpose training rooms, accessible fitness equipment, community meeting rooms and lounges, IKE Box Café, youth development spaces, and rooftop track and terrace"
          services="Childcare, group classes, personal training, community gatherings, camps, and youth and adult sports"
          />
        <PortfolioCard
          image={{ source: "/angeles-house.jpg", alt: "Exterior of the Angeles House in Los Angeles, CA" }}
          name="Angeles House"
          tagLine="Union Rescue Mission"
          tagLineLink="https://urm.org/"
          closingYear="2021"
          location="Los Angeles, CA"
          netProceeds="$7.9 Million"
          budget="$39.6 Million"
          CDEs="LA Development Fund, Genesis LA, and New Markets Community Capital (TELACU)"
          investor="Wells Fargo"
          impacts="Shelter and Services for 130 Families Annually"
          features="86 transitional housing units with 364 total beds, medical and dental clinic, computer lab, classroom, daycare, administrative and case management offices, and multipurpose room"
          services="Recovery programs, education, transportation, legal assistance, job training and placement, and mental health treatment"
          awards="2022 ENR California Best Projects Award of Merit - Residential/Hospitality"
          />
        <PortfolioCard
          image={{ source: "/mobile-loaves.jpg", alt: "Overhead View of Mobile Loaves & Fishes" }}
          name="Community First! Village Phase 2"
          tagLine="Mobile Loaves & Fishes"
          tagLineLink="https://mlf.org/"
          closingYear="2020"
          location="Austin, TX"
          netProceeds="$2.2 Million"
          budget="$18 Million"
          CDEs="PeopleFund and Urban Development Fund"
          investor="Chase Bank"
          impacts="Tiny Home Community with 310 Homes for Formerly Homeless"
          features="207 tiny homes, 103 park model homes, 7 outdoor kitchens, 7 laundry and bathroom facilities, community gardens, hydroponics, entrepreneur hub, woodworking shop, art house, community building, outdoor cinema, community inn, market, health resource center, and walking trails"
          services="Counseling, addiction recovery, financial literacy, legal services, education services, job training, and community events"
          awards="2024 Urban Land Institute Impact Award, AIA 2022 Community Vision Award, Engineering News Report 2016 Best Residential & Hospitality Development in the U.S."
          />
        <PortfolioCard
          image={{ source: "/towne-twin-village.jpg", alt: "Exterior view of Towne Twin Villages Under Construction" }}
          name="Towne Twin Village"
          tagLine="Housing First Community Coalition"
          tagLineLink="https://housingfirstsa.org/"
          closingYear="2022"
          location="San Antonio, TX"
          netProceeds="$3.1 Million"
          budget="$20.2 Million"
          CDEs="Accion Opportunity Fund, Capital Impact Partners, and DV Community Investment"
          investor="Valley National Bank Corporation"
          impacts="Tiny Home Community with 59 Homes and Services for Chronically Homeless Seniors"
          features="59 tiny homes, community kitchen, laundry facilities, hospitality house, administration and resource center, office spaces, health and dental clinic, hospitality house, chapel, convenience store, transportation pavilion, and spaces for partner organizations"
          services="Primary care, dental exams, mental health assessments, barbershop, nail and foot care, meals, addiction services, community events, exercise spaces, library, group therapy, and life skills training"
          awards="Novogradac 2023 Metro QLICI of the Year Honorable Mention"
          />
        <PortfolioCard
          image={{ source: "/restoration-house.jpg", alt: "Exterior of the Restoration House in Atlanta, GA" }}
          name="Restoration House"
          tagLine="Atlanta Mission"
          tagLineLink="https://atlantamission.org/"
          closingYear="2020"
          location="Atlanta, GA"
          netProceeds="$3.0 Million"
          budget="$17.5 Million"
          CDEs="CAHEC New Markets, Invest Atlanta, and Truist Community Development Enterprises"
          investor="Truist"
          impacts="Low Barrier Shelter for Women and Children"
          features="102 bed capacity with space for a Federally Qualified Health Care provider, childcare area, classrooms, kitchen, communal dining, conference rooms, administrative offices, volunteer work rooms, and donation drop off space"
          services="Healthcare provided by Mercy Health, children's activities, clothing closet, counseling, employment training, life skills training, and resource referrals"
          awards="2022 Urban Land Institute Atlanta Excellence in Civic/Institutional Award"
          />
        <PortfolioCard
          image={{ source: "/voeghtly-shelter.jpg", alt: "Exterior of the Voeghtly Shelter in Pittsburgh, PA" }}
          name="Voeghtly Shelter and Ridge Place"
          tagLine="Light of Life Rescue Mission"
          tagLineLink="https://www.lightoflife.org/"
          closingYear="2020 and 2022"
          location="Pittsburgh, PA"
          netProceeds="$3.6 Million"
          budget="$21.6 Million"
          CDEs="Pittsburgh Urban Initiatives and PNC Community Partners"
          investor="PNC Bank"
          impacts="Phased Shelter Expansion with 110 Bed Capacity"
          features="Voeghtly Shelter: 24,607 SF with 50 beds; Ridge Place expansion: 56,185 SF with 60 beds. Amenities including kitchens, chapels, playrooms, classrooms, computer labs, training areas, counseling offices, food pantry, donation center, and green space"
          services="Recovery programs, case management, literacy and parenting classes, life skills and computer training, daycare, after school programs, housing referrals, job training, employment placement, internships, and connections to community resources"
          />
        <PortfolioCard
          image={{ source: "/tommy-car-wash.jpg", alt: "Ribbon Cutting Ceremony for the Headquarters and Production Center of Tommy Car Wash Systems" }}
          name="Headquarters and Production Center"
          tagLine="Tommy Car Wash Systems"
          tagLineLink="https://tommycarwash.com/"
          closingYear="2021"
          location="Holland, MI"
          netProceeds="$5.4 Million"
          budget="$35.3 Million"
          CDEs="Michigan Community Capital, Cinnaire New Markets, and Old National Bank"
          investor="Old National Bank"
          impacts="146 FTE Jobs Created from 245,000 SF Expansion"
          features="New headquarters and manufacturing facility with production and assembly space, onsite demonstration car wash, remote monitoring for hundreds of car wash locations, and corporate office space"
          results="146 FTE jobs created, 155 FTE jobs retained, and 157 FTE construction jobs. Employee recruitment through partnerships with local organizations, and training and advancement opportunities for employees"
        />
        <PortfolioCard
          image={{ source: "/mel-trotter.jpg", alt: "Exterior of 101 Garden Street SE Location of Mel Trotter" }}
          name="Shelter and Next Step Job Training Center"
          tagLine="Mel Trotter Ministries"
          tagLineLink="https://www.meltrotter.org/"
          closingYear="2023"
          location="Grand Rapids, MI"
          netProceeds="$4.0 Million"
          budget="$27.3 Million"
          CDEs="Michigan Community Capital, Consortium America, Cinnaire New Markets, and Chase New Markets Corporation"
          investor="Chase Bank"
          impacts="Skilled Training for 66 and Support Services for 6,860 Annually"
          features="Downtown shelter with 116 transitional housing units, 400 emergency shelter beds, clinic, and support service space; 15,000 SF manufacturing training center; and 20,000 SF mixed-use building with 10 transitional workforce apartments, skilled job training program space, construction offices, and community space"
          services="Construction and workforce development program with hard and soft skills training, housing placement, legal assistance, life skills training, counseling, and medical/dental/vision/chiropractic services"
          />
        <PortfolioCard
          image={{ source: "/memphis-union-mission.jpg", alt: "Rear Facing Exterior of the Memphis Union Mission's Opportunity Center Phases 1 and 2" }}
          name="The Opportunity Center Phases 1 and 2"
          tagLine="Memphis Union Mission"
          tagLineLink="https://www.memphisunionmission.org/"
          closingYear="2019 and 2022"
          location="Memphis, TN"
          netProceeds="$6.9 Million"
          budget="$36.6 Million"
          CDEs="Hope Enterprise Corporation, Brownfield Revitalization, and Capital One Community Renewal Fund"
          investor="Capital One"
          impacts="226 Bed Emergency Shelter with Supportive Services"
          features="226 emergency shelter beds, medical and dental service space, auditorium, clothing distribution area, activity area, lounges, a library, counseling rooms, classroom, community gathering spaces, and offices"
          services="Recovery programs, counseling, life skills training, education, employment readiness, haircuts, medical and dental services, non-prescription reading glasses, podiatry evaluations, and health screenings"
          awards="2022 Memphis Business Journal - Building Memphis award winner"
          />
        <PortfolioCard
          image={{ source: "/helping-up-mission.jpg", alt: "Exterior of the Helping Up Mission's Center for Women and Children" }}
          name="Center for Women & Children"
          tagLine="Helping Up Mission"
          tagLineLink="https://helpingupmission.org/"
          closingYear="2021"
          location="Baltimore, MD"
          netProceeds="$3 Million"
          budget="$50 Million"
          CDEs="CAHEC New Markets and Harbor Bankshares Corporation"
          investor="M&T Bank and Chase Bank"
          impacts="Programs Serving 300 Women and 50 Children Annually"
          features="Seven story facility with 260 emergency and transitional housing beds, classrooms, childcare, primary care, workforce development center, recreation area, counseling rooms, offices, and Enterprise Green Communities features including green roofs, electric car charging stations, and environmentally conscious finish materials"
          services="Case management, drug and alcohol counseling, job and life skills training, work therapy, financial literacy, dental/vision/health care, mental health services, GED training and testing, after school childcare and academic support, and aftercare"
          awards="Baltimore Business Journal 2021 CFO Award"
          />
        <PortfolioCard
          image={{ source: "/merced-rescue-mission.jpg", alt: "Exterior of Merced Rescue Mission's Village of Hope finishing construction" }}
          name="Village of Hope"
          tagLine="Merced Rescue Mission"
          tagLineLink="https://www.mercedcountyrescuemission.org/"
          closingYear="2020"
          location="Merced, CA"
          netProceeds="$1.2 Million"
          budget="$8.6 Million"
          CDEs="Central Valley NMTC Fund"
          investor="Wells Fargo"
          impacts="Homeless Respite, Veteran, and Family Housing for over 52 individuals"
          features="Respite housing with 32 beds and a clinic, 10 apartment-style units for veterans, 10 apartment-style units for families, administrative space, commercial kitchen and shared cooking areas, resident lounges, and a workshop and storage building"
          services="Respite care for homeless individuals discharged from the hospital, healthcare services, counseling, substance abuse recovery, job and life skills training, parenting classes, financial literacy, support groups, housing referrals, and clothing assistance"
          awards="Named a 2020 California Nonprofit of the Year by CalNonprofits"
          />
        <PortfolioCard
          image={{ source: "/coastal-container.jpg", alt: "Exterior of the Coastal Container Manufacturing Equipment and Rail Spur Project" }}
          name="Manufacturing Equipment and Rail Spur"
          tagLine="Coastal Container"
          tagLineLink="https://coastal-container.com/"
          closingYear="2023"
          location="Holland, MI"
          netProceeds="$4 Million"
          budget="$23 Million"
          CDEs="Mascoma Community Development, Evernorth Rural Ventures, HEDC New Markets, and Chase New Markets Corporation"
          investor="Chase Bank"
          impacts="99 Created and 105 Retained FTE Jobs from 60,235 SF Expansion"
          features="Packaging solutions company expansion adding rail spur and vertical integration of corrugation system that will increase operational efficiency, decrease material costs, and insulate operations from supply chain constraints"
          results="204 high quality jobs with benefits and training opportunities, English as a Second Language classes for employees, reduced diesel fuel consumption and road wear by replacing trucking with railway, new wastewater pretreatment equipment, and new scrap and dust collection system reducing natural gas usage"
          />
        <PortfolioCard
          image={{ source: "/bridge-of-grace.jpg", alt: "Concept art of the Bridge of Grace project" }}
          name="Early Years Academy and Health Clinic"
          tagLine="Bridge of Grace"
          tagLineLink="https://www.bridgeofgracecmc.org/"
          closingYear="2023"
          location="Fort Wayne, IN"
          netProceeds="$2.7 Million"
          budget="$15.3 Million"
          CDEs="Fort Wayne New Markets Revitalization Fund and Cinnaire New Markets"
          investor="Old National Bank"
          impacts="Health Clinic and Learning Center Serving 630 Individuals Annually"
          features="Early Years Academy learning center with age specific classrooms, health clinic, office space, and community meeting space"
          services="Early childhood learning for 120 children ages 0-5 annually, before and after school care for 30 school-age children annually, medical services, job training, financial and homeownership counseling, small business support, and community events"
          />
          <PortfolioCard
          image={{ source: "/nashville-rescue.jpg", alt: "Exterior of the Nashville Rescue Mission" }}
          name="Women's and Children's Center"
          tagLine="Nashville Rescue Mission"
          tagLineLink="https://nashvillerescuemission.org/"
          closingYear="2023"
          location="Nashville, TN"
          netProceeds="$2.0 Million"
          budget="$22.0 Million"
          CDEs="Pathway Lending and PNC Community Partners"
          investor="PNC Bank"
          impacts="Shelter & Recovery Program for 2,526 People Annually"
          features="Emergency shelter with 348 beds, recovery program with 52 beds, auditorium, classrooms, day rooms, kids play area, cafeteria, and office and meeting space to serve women and their children"
          services="Counseling, addiction recovery, mental health services, therapeutic intervention, case management, coping skills development, life skills classes, GED completion assistance, work therapy, and connections to employment and stable housing"
          />
          <PortfolioCard
          image={{ source: "/dallas-life.jpg", alt: "Exterior of the Dallas LIFE Homeless Recovery Center" }}
          name="Homeless Recovery Center"
          tagLine="Dallas Mission for LIFE"
          tagLineLink="https://dallaslife.org/"
          closingYear="2020"
          location="Dallas, TX"
          netProceeds="$1.4 Million"
          budget="$12.5 Million"
          CDEs="Urban Development Fund"
          investor="Wells Fargo"
          impacts="Shelter & Recovery Program with 300 Bed Capacity"
          features="109,540 SF rehabilitation with 300 beds, commercial kitchen, communal dining room, health clinic, daycare, classrooms, counseling rooms, barber space, clothing exchange, convenience store, and office and administrative space"
          services="Medical/dental/vision services, job placement, daycare, education, substance abuse recovery program, mental health services, counseling, legal aid, and Veterans Administration assistance"
          />
      </div>
      <p className="my-3 text-center">In partnership with Capital for Compassion</p>
      <hr className="border border-4 border-primary rounded-pill" />
    </>
  );
}
