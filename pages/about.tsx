import Head from 'next/head';

export default function About() {
  const title = 'EBS Tax Credits - About Us';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="Learn more about EBS Tax Credits" />
      </Head>
      <h1 className="mt-3 text-center fw-bold">About EBS Tax Credits</h1>
      <br />
      <h2>EBS is...</h2>
      <ul>
        <li>A team that has delivered over $450 million of NMTC allocations since 2016.</li>
        <li>A company with a passion for nonprofit community facilities.</li>
        <li>An advisor with interest in tiny home, education, health, and manufacturing projects with a compelling community story.</li>
        <li>An experienced consultant with relationships with nearly every relevant Community Development Entity.</li>
        <li>A firm with a national footprint of projects, from California to Connecticut and North Dakota to Texas.</li>
      </ul>
      <h2>EBS Tax Credits' Role</h2>
      <p>
        EBS's role in your project is to evaluate the NMTC opportunity and likelihood of funding, develop the marketing
        materials to use with Community Development Entities (CDEs) and investors, raise awareness among CDEs, complete
        intakes from interested CDEs, secure a capital campaign bridge loan (if needed) that is compatible with the NMTC
        structure, assist with other funding source applications as needed, secure sub-allocations of NMTCs from CDEs,
        select the tax credit investor, structure the NMTC transaction, and lead through the NMTC closing process. EBS
        can additionally manage the monthly draws of the NMTC funds after the closing, and occasionally act as the
        owner's representative or project manager.
      </p>
      <h4>EBS Tax Credits' Steps in this role</h4>
      <ol>
        <li>Evaluate the NMTC opportunity and likelihood of funding.</li>
        <li>Meet with the project's key decision-makers including the Board of Directors.</li>
        <li>
          Raise awareness of your project among CDEs by developing marketing materials, making personal contacts, and
          completing CDE application forms.
        </li>
        <li>Secure allocation from CDE(s) in a very competitive field.</li>
        <li>Guide the selection of a tax credit investor.</li>
        <li>Assemble the team to manage all aspects of this complex transaction.</li>
        <li>Assure that the NMTC and other funding sources are compatible.</li>
        <li>
          Provide leadership and coordination throughout the NMTC closing process, including the identification of open
          issues and assistance resolving them.
        </li>
        <li>Secure interim financing, compatible with NMTCs (optional).</li>
        <li>Manage monthly draws of NMTC funds post-closing (optional).</li>
        <li>Support preparation of materials for annual reporting (optional).</li>
        <li>Act as the owner's representative or project manager (optional).</li>
      </ol>
      <h2>The History of EBS</h2>
      <p>
        The first 7 years of EBS involved delivering Affordable Housing Program funds under contract to Foster and
        Associates (later Capital for Compassion). Some of those projects included NMTC funds. EBS helped integrate the
        two funding sources, often with the Community Funding Group, on 9 projects.
      </p>
      <p>
        The second phase of EBS included a partnership with Capital for Compassion. The team directly sourced and closed
        over $320 million of New Markets Tax Credits in 7 years from 46 sub-allocations across 24 closings,
        from 21 unique CDEs and 8 different investors.
      </p>
      <p>
        The third and current phase of EBS, building on this foundation, is to continue delivering projects high in
        impacts through efficient closings, broadening the scope of community facilities served across the country, and
        diversifying possible funding sources.
        In 2023 and 2024, EBS closed $118 million of New Markets Tax Credits for 7 projects with 12 separate CDEs.
        We are excited to see growth in our communities over the next 7 years and beyond!
      </p>
    </>
  );
}
