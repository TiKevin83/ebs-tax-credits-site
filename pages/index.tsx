import Head from 'next/head';
import Link from 'next/link';

export default function Home() {
  const title = 'EBS Tax Credits - Home Page';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="Get to know EBS Tax Credits" />
      </Head>
      <h1 className="mt-3 text-center" style={{ color: '#2F62AE' }}>
        EBS Tax Credits
      </h1>
      <h2 className="mt-3 text-center"> Translating Tax Credits into Funding</h2>
      <br />
      <p>
      Is your organization growing? Are you building a new community facility, or undertaking a major renovation/expansion? Are you trying to complete a capital campaign, or concerned about ramping up annual fundraising for a larger operating budget? Have construction costs escalated and you urgently need to close the funding gap? Perhaps EBS Tax Credits has your solution.
      </p>
      <hr />
      <p>
      EBS delivers funding for various types of facilities including homeless services, workforce development, early childhood development, health care, community food, nonprofit incubators and hubs, education, addiction recovery, transitional housing, and community recreation. We have a particular interest and expertise in tiny home communities.  Manufacturing expansions are also possible, with a significant expansion of employment and a compelling community connection.
      </p>
      <hr />
      <p>
        EBS specializes in New Markets Tax Credits, State New Markets Tax Credits, and the Federal Home Loan Banks' Affordable Housing Program.
        EBS enjoys complicated, highly technical funding that requires unique expertise.
        We know the opportunities and limitations of combining these funding sources with each other as well as with capital campaigns, government grants such as ARPA/SLFRF, Low Income Housing Tax Credits, and other tax credits.
        Ultimately we manage the process so that your focus can remain on your capital campaign and/or operations.
      </p>
      <hr />
      <p>
        We enjoy getting to know what makes your place a unique community. We may be able to help anywhere in the United
        States, but the specific address of your site affects your eligibility. Learn more about your options by{' '}
        <Link href="/contact">contacting us.</Link>
      </p>
    </>
  );
}
