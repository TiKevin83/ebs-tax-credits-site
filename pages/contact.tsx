import Head from 'next/head';

export default function Contact() {
  const title = 'EBS Tax Credits - Contact';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="Contact EBS Tax Credits" />
      </Head>
      <h1 className="mt-3 text-center">
        <span className="fw-bold">Contact</span> EBS Tax Credits
      </h1>
      <div className="row">
        <div className="col-md-6">
          <div className="card mb-3">
            <div className="row g-0">
              <div className="col-md-4">
                <img src="/john-headshot.jpg" className="img-fluid rounded-start" alt="..." />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">John McGeehan</h5>
                  <p className="text-muted">President of Operations</p>
                  <p className="card-text">
                    <a className="card-link" href="mailto:john@ebstaxcredits.com">
                      John@EBStaxcredits.com
                    </a>
                    <br />
                    <a className="card-link" href="tel:616-990-6511">
                      616-990-6511
                    </a>
                    <br />
                    <br />
                    430 E 8th St Suite #269
                    <br />
                    Holland, MI  49423
                    <br />
                    <br />
                    <img
                      srcSet="/WideLogo-nav.png 116w, /WideLogo-nav-2x.png 232w"
                      height="60px"
                      width="116px"
                      alt="EBS Tax Credits Logo"
                    />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 d-flex justify-content-center align-items-center">
          <h2 className="fs-1">Let's talk about closing the funding gap on your project!</h2>
        </div>
      </div>
    </>
  );
}
