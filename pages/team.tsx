import Head from 'next/head';
import Link from 'next/link';

export default function Team() {
  const title = 'EBS Tax Credits - Our Team';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta property="og:description" content="Meet the EBS Tax Credits Team" />
      </Head>
      <h1 className="mt-3 text-center">Meet the EBS Tax Credits Team</h1>
      <br />
      <div className="row row-cols-1 row-cols-md-2 g-4">
        <div className="col">
          <div className="card h-100">
            <div className="row g-0">
              <div className="col-md-4">
                <img src="/john-headshot.jpg" className="img-fluid rounded-start" alt="Headshot of John McGeehan" />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">John McGeehan</h5>
                  <p className="text-muted">President of Operations</p>
                  <p className="card-text">
                    John McGeehan leads EBS's tax credit efforts. John graduated from the University of Delaware and
                    earned a Master's Degree from the University of Michigan. After 20 years as an engineer and project
                    manager in the automotive industry, John transferred those skills to Community Development. John now
                    has 15 years of experience delivering funding for services and affordable housing in predominantly
                    low-income communities across the county. John has secured over $500 million of tax credit allocations and $100 million in affordable housing funding mostly across all Federal Home Loan Banks.
                  </p>
                  <Link className="card-link" href="/contact">
                    Contact John
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card h-100">
            <div className="row g-0">
              <div className="col-md-4">
                <img src="/maggie-headshot.jpg" className="img-fluid rounded-start" alt="Headshot of Margaret Assink" />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">Maggie Assink</h5>
                  <p className="text-muted">
                    Principal at{' '}
                    <a className="card-link" href="https://www.linkedin.com/company/mast-development/">
                      MAST Development
                    </a>
                  </p>
                  <p className="card-text">
                    Maggie provides clients with project management services directly through MAST Development,
                    and is also a Project Manager for EBS. She leads the collection of data, prior costs, and other required material,
                    and brings an acute attention to detail as she manages task checklists and budget fluctuations through the closing process.
                    Post-closing, Maggie guides clients through disbursing
                    NMTC funds and completing annual reporting. Her previous experience includes grant writing,
                    executive assistant duties, and coordinating media partnerships and corporate sponsorships. Maggie
                    has a bachelor's degree in Political Science from the University of Michigan.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card h-100">
            <div className="row g-0">
              <div className="col-md-4">
                <img src="/steve-headshot.jpg" className="img-fluid rounded-start" alt="Headshot of Steve Dykstra" />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">Steve Dykstra</h5>
                  <p className="text-muted">
                    Principal at{' '}
                    <a href="https://www.linkedin.com/company/advantage-development-solutions/about/">
                      Advantage Development Solutions
                    </a>
                  </p>
                  <p className="card-text">
                    Steve delivers project management and owner's representative services through Advantage Development
                    Solutions, and also provides his unique skillset through EBS. Steve has 40 years of experience that
                    customers can leverage to help meet the cost, timing, and quality expectations of their projects.
                    His background includes program management, engineering, acquisition, finance, and team leadership
                    roles in the automotive, testing, development, and construction industries. He has provided
                    leadership and coordination during the planning, financing, design, and construction phases of many
                    projects. Steve has an Engineering degree from General Motors Institute and ongoing experience as a
                    board member for multiple organizations.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
